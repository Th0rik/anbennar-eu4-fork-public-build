namespace = mage_organization


# Initialization - Decision Choosing
country_event = {
	id = mage_organization.1
	title = mage_organization.1.t
	desc = mage_organization.1.d
	picture = DHIMMI_ESTATE_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		has_estate = estate_mages
		
		NOT = { has_country_flag = init_mage_org_setup }
		
		NOT = { has_country_flag = banned_magic }
		NOT = { has_country_modifier = mage_organization_decentralized }
		NOT = { has_country_modifier = mage_organization_centralized }
		NOT = { has_country_modifier = mage_organization_magisterium }
	}
	
	immediate = {
		clear_mage_organization_effect = yes
	}
	
	#Decentralized
	option = {
		name = mage_organization.1.a
		ai_chance = {
			factor = 34
		}
		trigger = {
			NOT = { has_country_modifier = mage_organization_decentralized }
			is_part_of_hre = no	#for now
		}
		ai_chance = {
			factor = 0.1
			modifier = {
				factor = 10
				is_at_war = yes
			}
		}
		set_country_flag = mage_organization_decentralized_flag 
		add_country_modifier = { 
			name = mage_organization_decentralized 
			duration = -1 
		}
	}
	
	#Centralized
	option = {
		name = mage_organization.1.b
		ai_chance = {
			factor = 33
		}
		trigger = {
			NOT = { has_country_modifier = mage_organization_centralized }
			adm_tech = 12
		}
		set_country_flag = mage_organization_centralized_flag
		add_country_modifier = { 
			name = mage_organization_centralized 
			duration = -1 
		}
	}
	
	#Magisterium
	option = {
		name = mage_organization.1.c
		ai_chance = {
			factor = 33
			modifier = {
				factor = 100
				is_part_of_hre = yes
			}
		}
		trigger = {
			NOT = { has_country_modifier = mage_organization_magisterium }
			OR = {
				is_part_of_hre = yes
				if = {
					limit = {
						exists = A85
					}
					A85 = {
						has_opinion = {
							who = ROOT
							value = 100
						}
					}
				}
			}
		}
		set_country_flag = mage_organization_magisterium_flag
		add_country_modifier = { 
			name = mage_organization_magisterium
			duration = -1 
		}
	}
	
	option = {
		name = mage_organization.1.dd
		ai_chance = {
			factor = 1
		}
		trigger = {
			OR = {
				religion = ravelianism
			}
		}
		set_country_flag = banned_magic
	}
}

#Expel Mages Change Noble Reform
country_event = {
	id = mage_organization.2
	title = mage_organization.2.t
	desc = mage_organization.2.d
	picture = NOBLE_ESTATE_DEMANDS_eventPicture
	
	mean_time_to_happen = {
		months = 1
	}
	
	trigger = {
		has_country_modifier = expelled_mages
		has_reform = magical_elite_reform
		has_estate = estate_nobles
	}
	
	#Strengthen Nobles
	option = {
		name = mage_organization.2.a
		ai_chance = {
			factor = 50
		}
		add_country_modifier = {
			name = magic_noble_reshuffle
			duration = 1825
		}
		add_government_reform = enforce_privileges_reform 
	}
	
	#Curtail Nobles
	option = {
		name = mage_organization.2.b
		ai_chance = {
			factor = 50
			modifier = {
				factor = 0.5
				is_at_war = yes
			}
		}
		random_owned_province = {
			limit = {
				has_estate = estate_nobles
			}
			spawn_rebels = {
				size = 2
				leader = noble_zealot
				type = noble_rebels
			}
		}
		

		add_government_reform = quash_noble_power_reform 
	}
}