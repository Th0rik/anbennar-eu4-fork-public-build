#These are Generic missions, available to all tags that don't have an override.

#Below are mission series, within them are the actual missions.

sugamber_missions = { 
	slot = 1 
	generic = no
	ai = yes 
	potential = {
	tag = A48
	} #These are triggers for the entire series
	
	
	
	sugamber_resolve_dispute = { 
		icon = mission_monarch_in_throne_room
		required_missions = {  } 
		trigger = {
		has_country_flag = had_succession_war
		}
		effect = { 
			add_country_modifier = {
				name = "undisputed_dynasty" #0.5 yearly legitimacy
				duration = -1 #Forever
			}
		}
	}
	
	sugamber_urban_development = {
		icon = mission_hanseatic_city
		required_missions = {  }
		provinces_to_highlight = {
			OR = {
				province_id = 918
				province_id = 410
				province_id = 411
				province_id = 409
				province_id = 315
				province_id = 319
				province_id = 913
			}
		}
		trigger = {  
		918 = { development = 10 }
		410 = { development = 10 }
		411 = { development = 10 }
		409 = { development = 10 }
		315 = { development = 10 }
		319 = { development = 10 }
		913 = { development = 10 }
		}
		effect = {
		919 = { 
		center_of_trade = 2 
		add_base_tax = 2
		add_base_production = 2
		add_base_manpower = 1
		change_trade_goods = paper
		}
		}
		}
		
	
	sugamber_conquer_bisan = {
		icon = mission_landsknecht_soldier
		required_missions = {  }
		provinces_to_highlight = {
			OR = {
				province_id = 579
				province_id = 300
				province_id = 923
				province_id = 332
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {  
		579 = { country_or_non_sovereign_subject_holds = ROOT }
		300 = { country_or_non_sovereign_subject_holds = ROOT }
		923 = { country_or_non_sovereign_subject_holds = ROOT }
		332 = { country_or_non_sovereign_subject_holds = ROOT }
		}
		effect = {
			vassalize = Z02
			}
		}
	
	
	sugamber_conquer_cellianade = {
		icon = mission_cossack_cavalry
		required_missions = { sugamber_conquer_bisan }
		provinces_to_highlight = {
			OR = {
				province_id = 414
				province_id = 416
				province_id = 412
				province_id = 413
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
		414 = { country_or_non_sovereign_subject_holds = ROOT }
		416 = { country_or_non_sovereign_subject_holds = ROOT }
		412 = { country_or_non_sovereign_subject_holds = ROOT }
		413 = { country_or_non_sovereign_subject_holds = ROOT }
		}
		effect = {
		vassalize = A86	
		}
		}
	
	
	sugamber_claim_eastern_front = {
		icon = mission_italian_condottiere
		required_missions = { sugamber_conquer_cellianade }
		provinces_to_highlight = {
			OR = {
				province_id = 420
				province_id = 922
				province_id = 921
				province_id = 313
				province_id = 904
				province_id = 418
				province_id = 419
				province_id = 325
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
		arannen_area = { country_or_non_sovereign_subject_holds = ROOT }
		420 = { country_or_non_sovereign_subject_holds = ROOT }
		922 = { country_or_non_sovereign_subject_holds = ROOT }
		921 = { country_or_non_sovereign_subject_holds = ROOT }
		}
		effect = {
		ROOT = {
		add_adm_power = 100
		add_dip_power = 100
		add_mil_power = 100
		}
			}
		}
	}



