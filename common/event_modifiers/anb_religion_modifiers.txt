
removed_corinite_centers = {
	tolerance_heretic = -2
}
# Great Dookan Abilities
dookan_reconnected_with_past_lives = {
	land_morale = 0.2
	shock_damage = 0.1
	shock_damage_received = 0.1
}
dookan_gather_great_host = {
	reinforce_speed = 0.33
	global_regiment_recruit_speed = -0.33
}
dookan_anointed_slave_herd = {
	local_development_cost = -0.1
	local_production_efficiency = 0.1
}

# The Hunt Abilities
the_hunt_fair_winds = {
	movement_speed = 0.3
	shock_damage_received = -0.2
}

# Harmonizing religions
harmonized_cannorian = {
	tolerance_own = 1
	religion = yes
}

harmonized_bulwari = {
	yearly_harmony = 1
	religion = yes
}

harmonized_goblin = {
	global_institution_spread = 0.2
	religion = yes
}

harmonized_kheteratan = {
	defensiveness = 0.1
	religion = yes
}

harmonized_raheni = {
	idea_cost = -0.07
	religion = yes
}

harmonized_gnollish = {
	harsh_treatment_cost = -0.1
	religion = yes
}

harmonized_dwarven = {
	production_efficiency = 0.05
	religion = yes
}

harmonized_elven = {
	prestige_decay = -0.01
	religion = yes
}

harmonized_gerudian = {
	prestige = 0.5
	religion = yes
}

harmonized_orcish = {
	infantry_power = 0.05
	religion = yes
}

harmonized_dragon_cult = {
	movement_speed = 0.1
	religion = yes
}

harmonized_aelantiri = {
	stability_cost_modifier = -0.05
	religion = yes
}

harmonized_harpy_cults = {
	leader_land_shock = 1
	religion = yes
}

harmonized_effelai = {
	land_attrition = -0.1
	religion = yes
}

harmonized_andic = {
	trade_efficiency = 0.1
	religion = yes
}

harmonized_taychendi = {
	advisor_pool = 1
	religion = yes
}

harmonized_ynnic = {
	reinforce_speed = 0.1
	religion = yes
}

harmonized_fey_religion = {
	hostile_attrition = 0.5
	religion = yes
}

harmonized_godlost = {
	discipline = 0.025
	religion = yes
}
