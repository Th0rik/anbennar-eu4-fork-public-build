defined_text = {
    name = strikeTheEarth
	random = yes

    text = {
        localisation_key = strikeTheEarth_1
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = strikeTheEarth_2
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = strikeTheEarth_3
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = strikeTheEarth_4
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = strikeTheEarth_5
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = strikeTheEarth_6
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = strikeTheEarth_7
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = strikeTheEarth_8
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = strikeTheEarth_9
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = strikeTheEarth_10
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = strikeTheEarth_11
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = strikeTheEarth_12
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = strikeTheEarth_13
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = strikeTheEarth_14
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = strikeTheEarth_15
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = strikeTheEarth_16
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = strikeTheEarth_17
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = strikeTheEarth_18
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = strikeTheEarth_19
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = strikeTheEarth_20
        trigger = {
            always = yes
        }
    }
}

defined_text = {
    name = swearAtTheEarth
	random = yes

    text = {
        localisation_key = swearAtTheEarth_1
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = swearAtTheEarth_2
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = swearAtTheEarth_3
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = swearAtTheEarth_4
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = swearAtTheEarth_5
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = swearAtTheEarth_6
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = swearAtTheEarth_7
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = swearAtTheEarth_8
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = swearAtTheEarth_9
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = swearAtTheEarth_10
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = swearAtTheEarth_11
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = swearAtTheEarth_12
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = swearAtTheEarth_13
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = swearAtTheEarth_14
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = swearAtTheEarth_15
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = swearAtTheEarth_16
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = swearAtTheEarth_17
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = swearAtTheEarth_18
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = swearAtTheEarth_19
        trigger = {
            always = yes
        }
    }
	text = {
        localisation_key = swearAtTheEarth_20
        trigger = {
            always = yes
        }
    }
}

defined_text = {
    name = FederationStanding
	random = no

	text = {
        localisation_key = under_foreign_occupation
        trigger = {
            is_subject = yes
			overlord = { NOT = { has_country_modifier = lake_federation_member } }
        }
    }
	text = {
        localisation_key = under_occupation
        trigger = {
            is_subject = yes
        }
    }
    text = {
        localisation_key = lake_leader
        trigger = {
            NOT = {
				calc_true_if = {
					all_known_country = {
						check_variable = {
							which = FederationPoints
							which = PREV
						}
						is_subject = no
					}
					amount = 1
				}
			}
			is_subject = no
        }
    }
	text = {
        localisation_key = lake_very_high_standing
        trigger = {
			NOT = {
				calc_true_if = {
					all_known_country = {
						check_variable = {
							which = FederationPoints
							which = PREV
						}
					}
					amount = 4
				}
			}
        }
    }
	text = {
        localisation_key = lake_high_standing
        trigger = {
			NOT = {
				calc_true_if = {
					all_known_country = {
						check_variable = {
							which = FederationPoints
							which = PREV
						}
					}
					amount = 8
				}
			}
        }
    }
	text = {
        localisation_key = lake_normal_standing
        trigger = {
			NOT = {
				calc_true_if = {
					all_known_country = {
						check_variable = {
							which = FederationPoints
							which = PREV
						}
					}
					amount = 15
				}
			}
        }
    }
	text = {
        localisation_key = lake_low_standing
        trigger = {
			NOT = {
				calc_true_if = {
					all_known_country = {
						check_variable = {
							which = FederationPoints
							which = PREV
						}
					}
					amount = 25
				}
			}
        }
    }
	text = {
        localisation_key = lake_very_low_standing
        trigger = {
            always = yes
        }
    }
}


defined_text = {
    name = FederationStatus
	random = no

    text = {
        localisation_key = federation_military
        trigger = {
            has_country_modifier = federation_fighter
        }
    }
	
	text = {
        localisation_key = federation_financer
        trigger = {
            has_country_modifier = federation_subventionner
        }
    }
	
	text = {
        localisation_key = federation_leecher
        trigger = {
			has_global_flag = lake_the_place_of_everyone
        }
    }
}

defined_text = {
    name = FederationWarMandate
	random = no

    text = {
        localisation_key = have_war_mandate
        trigger = {
            has_country_modifier = federation_war_mandat
        }
    }
	
	text = {
        localisation_key = dont_have_war_mandate
        trigger = {
            NOT = { has_country_modifier = federation_war_mandat }
        }
    }
}

defined_text = {
    name = FederationLeague
	random = no

    text = {
        localisation_key = federation_blue_league
        trigger = {
            has_country_flag = federation_blue
        }
    }
	
	text = {
        localisation_key = federation_red_league
        trigger = {
            has_country_flag = federation_red
        }
    }
	
	text = {
        localisation_key = federation_yellow_league
        trigger = {
            has_country_flag = federation_yellow
        }
    }
	
	text = {
        localisation_key = federation_green_league
        trigger = {
            has_country_flag = federation_green
        }
    }
}


##########
##Lake Federation Constitution Loca
##########
defined_text = {
    name = FederationArticle1
	random = no
	
	text = {
        localisation_key = federation_mil_article_1
        trigger = {
            has_country_flag = federation_mil_article_1
        }
    }
	
	text = {
        localisation_key = federation_trade_article_1
        trigger = {
            has_country_flag = federation_trade_article_1
        }
    }
	
	text = {
        localisation_key = federation_industry_article_1
        trigger = {
            has_country_flag = federation_industry_article_1
        }
    }
}

defined_text = {
    name = FederationArticle2
	random = no
	
	text = {
        localisation_key = federation_mil_article_2
        trigger = {
            has_country_flag = federation_mil_article_2
        }
    }
	
	text = {
        localisation_key = federation_trade_article_2
        trigger = {
            has_country_flag = federation_trade_article_2
        }
    }
	
	text = {
        localisation_key = federation_industry_article_2
        trigger = {
            has_country_flag = federation_industry_article_2
        }
    }
}

defined_text = {
    name = FederationArticle3
	random = no
	
	text = {
        localisation_key = federation_mil_article_3
        trigger = {
            has_country_flag = federation_mil_article_3
        }
    }
	
	text = {
        localisation_key = federation_trade_article_3
        trigger = {
            has_country_flag = federation_trade_article_3
        }
    }
	
	text = {
        localisation_key = federation_industry_article_3
        trigger = {
            has_country_flag = federation_industry_article_3
        }
    }
}

defined_text = {
    name = FederationArticle4
	random = no
	
	text = {
        localisation_key = federation_mil_article_4
        trigger = {
            has_country_flag = federation_mil_article_4
        }
    }
	
	text = {
        localisation_key = federation_trade_article_4
        trigger = {
            has_country_flag = federation_trade_article_4
        }
    }
	
	text = {
        localisation_key = federation_industry_article_4
        trigger = {
            has_country_flag = federation_industry_article_4
        }
    }
}

defined_text = {
    name = FederationArticle5
	random = no
	
	text = {
        localisation_key = federation_mil_article_5
        trigger = {
            has_country_flag = federation_mil_article_5
        }
    }
	
	text = {
        localisation_key = federation_trade_article_5
        trigger = {
            has_country_flag = federation_trade_article_5
        }
    }
	
	text = {
        localisation_key = federation_industry_article_5
        trigger = {
            has_country_flag = federation_industry_article_5
        }
    }
}

defined_text = {
    name = FederationArticle6
	random = no
	
	text = {
        localisation_key = federation_mil_article_6
        trigger = {
            has_country_flag = federation_mil_article_6
        }
    }
	
	text = {
        localisation_key = federation_trade_article_6
        trigger = {
            has_country_flag = federation_trade_article_6
        }
    }
	
	text = {
        localisation_key = federation_industry_article_6
        trigger = {
            has_country_flag = federation_industry_article_6
        }
    }
}

defined_text = {
    name = FederationArticle7
	random = no
	
	text = {
        localisation_key = federation_mil_article_7
        trigger = {
            has_country_flag = federation_mil_article_7
        }
    }
	
	text = {
        localisation_key = federation_trade_article_7
        trigger = {
            has_country_flag = federation_trade_article_7
        }
    }
	
	text = {
        localisation_key = federation_industry_article_7
        trigger = {
            has_country_flag = federation_industry_article_7
        }
    }
}

defined_text = {
    name = FederationArticle8
	random = no
	
	text = {
        localisation_key = federation_mil_article_8
        trigger = {
            has_country_flag = federation_mil_article_8
        }
    }
	
	text = {
        localisation_key = federation_trade_article_8
        trigger = {
            has_country_flag = federation_trade_article_8
        }
    }
	
	text = {
        localisation_key = federation_industry_article_8
        trigger = {
            has_country_flag = federation_industry_article_8
        }
    }
}

defined_text = {
	name = KoboldHoard
	random = no
	
	text = {
		localisation_key = KoboldHoard1
		trigger = {
			REB = { check_variable = { dragon_cult_hoard = 75000 } }
		}
	}
	
	text = {
		localisation_key = KoboldHoard2
		trigger = {
			REB = { check_variable = { dragon_cult_hoard = 60000 } }
		}
	}
	
	text = {
		localisation_key = KoboldHoard3
		trigger = {
			REB = { check_variable = { dragon_cult_hoard = 50000 } }
		}
	}
	
	text = {
		localisation_key = KoboldHoard4
		trigger = {
			REB = { check_variable = { dragon_cult_hoard = 40000 } }
		}
	}
	
	text = {
		localisation_key = KoboldHoard5
		trigger = {
			REB = { check_variable = { dragon_cult_hoard = 30000 } }
		}
	}
	
	text = {
		localisation_key = KoboldHoard6
		trigger = {
			REB = { check_variable = { dragon_cult_hoard = 25000 } }
		}
	}
	
	text = {
		localisation_key = KoboldHoard7
		trigger = {
			REB = { check_variable = { dragon_cult_hoard = 20000 } }
		}
	}
	
	text = {
		localisation_key = KoboldHoard8
		trigger = {
			REB = { check_variable = { dragon_cult_hoard = 15000 } }
		}
	}
	
	text = {
		localisation_key = KoboldHoard9
		trigger = {
			REB = { check_variable = { dragon_cult_hoard = 10000 } }
		}
	}
	
	
	text = {
		localisation_key = KoboldHoard10
		trigger = {
			REB = { check_variable = { dragon_cult_hoard = 7500 } }
		}
	}
	
	text = {
		localisation_key = KoboldHoard11
		trigger = {
			REB = { check_variable = { dragon_cult_hoard = 0500 } }
		}
	}
	
	text = {
		localisation_key = KoboldHoard12
		trigger = {
			REB = { check_variable = { dragon_cult_hoard = 3000 } }
		}
	}
	
	text = {
		localisation_key = KoboldHoard13
		trigger = {
			REB = { check_variable = { dragon_cult_hoard = 1500 } }
		}
	}
	
	text = {
		localisation_key = KoboldHoard14
		trigger = {
			REB = { check_variable = { dragon_cult_hoard = 500 } }
		}
	}
	
	text = {
		localisation_key = KoboldHoard15
		trigger = {
			REB = { check_variable = { dragon_cult_hoard = 0 } }
		}
	}
}

