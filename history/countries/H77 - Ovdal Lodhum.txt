government = monarchy
add_government_reform = dwarven_clan_reform
government_rank = 1
primary_culture = garnet_dwarf
religion = ancestor_worship
technology_group = tech_dwarven
capital = 2914
add_country_modifier = {
	name = dwarven_administration
	duration = -1
}

1000.1.1 = { 
	set_country_flag = mage_organization_centralized_flag
	set_country_flag = dwarovar_remnant
	add_country_modifier = {
		name = remnant_legacy
		duration = -1
	}
}

1442.3.17 = {
	monarch = {
		name = "Gerin"
		dynasty = "Orcrend"
		culture = castellyrian
		religion = regent_court
		birth_date = 1389.11.4
		adm = 2
		dip = 3
		mil = 6
	}
}