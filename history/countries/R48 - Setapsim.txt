government = monarchy
add_government_reform = jamindar_reform
government_rank = 1
primary_culture = royal_harimari
add_accepted_culture = rabhidarubsad
religion = high_philosophy
technology_group = tech_harimari
religious_school = golden_palace_school
capital = 4427

1000.1.1 = { set_country_flag = mage_organization_centralized_flag }

1444.1.1 = {
	monarch = {
		name = "Dhasvas"
		dynasty = "of the Thousand Strikes"
		birth_date = 1399.8.7
		adm = 2
		dip = 2
		mil = 4
		culture = royal_harimari
	}
}