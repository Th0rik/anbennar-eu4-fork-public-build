government = theocracy
add_government_reform = monastic_order_reform
government_rank = 1
primary_culture = caamas
religion = summer_court
technology_group = tech_eordand
capital = 2037

1000.1.1 = { set_country_flag = mage_organization_decentralized_flag }

1426.3.10 = {
	monarch = {
		name = "Elthor"
		dynasty = "Longdream"
		adm = 3
		dip = 4
		mil = 3
		birth_date = 1410.8.1
	}
	add_ruler_personality = mage_personality
	set_ruler_flag = conjuration_1
	set_ruler_flag = conjuration_2
	set_ruler_flag = enchantment_1
	set_ruler_flag = enchantment_2
	add_ruler_personality = entrepreneur_personality
}